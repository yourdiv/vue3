// 导入 createApp 函数
import {createApp} from 'vue'

//导入需要挂载的组件
// import App from './App.vue'
import App from './components/myheader/App.vue'

import './index.css'

//创建 spa 实例 并挂载
createApp(App).mount('#app')
