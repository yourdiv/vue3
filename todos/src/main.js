import { createApp } from 'vue'
// import App from './App.vue'
// import App from './components/father-son/App.vue'
// import App from './components/brother/App.vue'
import App from './components/provide&inject/App.vue'
import './index.css'
import './assets/css/bootstrap.css'

createApp(App).mount('#app')
