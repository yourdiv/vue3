import { createApp } from 'vue'
import App from './App.vue'
// import './index.css'
// import App from './components/test/App.vue'

createApp(App).mount('#app')
